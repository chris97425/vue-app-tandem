import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Catalog from '@/components/Catalog'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Home',
      name: 'Home',
      component: Home
    },
    {
        path: '/Catalog',
        name: 'Catalog',
        component: Catalog
    }

      
  ]
})

